﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class Agentclick : MonoBehaviour {
    public Camera cam;
    public NavMeshAgent agent;
    public bool selected;
    public string selValue;



    void Update() {


    if (Input.GetKeyDown(selValue))
        {
            selected = !selected;
        }



        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit) && selected == true)
            {
                agent.SetDestination(hit.point);
                Debug.Log("Going to point, my sel value is " + selValue);
            }
        }
    }
}
