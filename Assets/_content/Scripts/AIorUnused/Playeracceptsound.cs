﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Playeracceptsound : MonoBehaviour
{
    public AudioClip under;
    AudioSource audioSource;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            audioSource.PlayOneShot(under, 0.7F);
            Debug.Log("Sound played");
        }
    }
}