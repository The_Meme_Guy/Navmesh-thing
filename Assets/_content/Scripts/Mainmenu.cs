﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Mainmenu : MonoBehaviour {

    public void PlayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void Quit()
    {
        Debug.Log("Application has quit");
        Application.Quit();
    }

    public void OpenGit()
    {
        // Application.OpenURL("https://github.com/990xan/Navmesh-thing");
        Debug.Log("opening github");
    }

}
