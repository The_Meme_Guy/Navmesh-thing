﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Accesslevelbar : MonoBehaviour {

    public Image self;
    [SerializeField]
    public GameObject player;
    private Color tmp;

    void Update()
    {
        if (player.GetComponent <Agentclick> ().selected)
        {
            Color tmp = self.GetComponent<Image>().color;
            tmp.a = 256f;
            self.GetComponent<Image>().color = tmp;
        } else
        {
            Color tmp = self.GetComponent<Image>().color;
            tmp.a = 0f;
            self.GetComponent<Image>().color = tmp;
        }
    }
}
