﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMover : MonoBehaviour {

    public float speed = 10f;

	void Update () {

        Vector3 translation = Input.GetAxis("Vertical") * Vector3.forward;
        translation += (Input.GetAxis("Horizontal") * Vector3.right);
        translation *= Time.deltaTime * speed;
        transform.position += translation;
    }
}
